# 爬虫赢商网购物中心数据

#### 介绍
爬虫赢商网购物中心数据，获取购物中心名字，城市，项目地，开发商，项目类型，商业建筑面积，项目楼层等。

#### 说明
python爬虫
1.根据链接爬取列表数据（如：名称，详细链接）。<br>
2.根据详细链接获取详情数据。<br>
3.保存数据到数据库。<br>
4.导出csv。<br>

### 技术应用
1.request 原生请求。<br>
2.psycopg2 连接数据库保存数据。<br>
3.BeautifulSoup 定位页面元素信息。<br>
4.ThreadPoolExecutor 多线程并发请求<br>
5.csv 导出csv <br>

### 导出csv文件下载
https://gitee.com/xudaile/ys_shopping_mall/raw/master/%E8%B4%AD%E7%89%A9%E4%B8%AD%E5%BF%83.csv

### 导出csv图
<div style="display:flex; height:auto; justify-content: center;">
<img src="https://gitee.com/xudaile/ys_shopping_mall/raw/master/images/%E8%B5%A2%E5%95%86%E7%BD%91%E8%B4%AD%E7%89%A9%E4%B8%AD%E5%BF%83.jpg" width="800px"/>
</div>